---
title: "A Contributor's Dream"
date: 2021-08-25T14:25:34+10:00
draft: false
Callout: "Combining a smart and efficient build system with a community of packagers"
---

Serpent OS combines a super fast packaging system with smart tooling to easily create new packages for Serpent OS.
With a thriving community of contributors they can quickly and efficiently test new changes and get them in front of
users. Serpent OS wants to reduce frictions and improve the efficiency of all contributors/developers. The
contributions of the community are pivotal to the success of many open source projects. With Serpent OS, the line
between being a user and a contributor has never been finer!

#### Lightning Fast Build System

Nobody likes to wait around for packages to build. In Serpent OS, we value your time (and ours!) so every effort is
made to make creating packages as fast and smooth as possible.

Maximising the performance of the clang compiler:
 - LTO and PGO built `clang`
 - Static `LLVM` in `clang` binary
 - Using `LLVM's` binutils variants (such as the more performant `lld` linker)

Combined these result to considerable performance benefits to `clang` and reduce what can be the longest part of
waiting for a build to finish.

{{<figure_screenshot_one image="../build_setup" caption="The parallel nature of Serpent OS helps minimize build times">}}

Unique jobs system integrated into `boulder` and `moss` where all tasks can run as soon as their prerequisites
are met including:
 - Fetching upstream tarballs while dependencies are calculated
 - Extracting packages and tarballs as soon as they are downloaded
 - Caching of packages means you only ever need to extract packages one time for your system and development needs

Utilizing pre-compiled headers (PCH) and unity builds:
 - Can result in significant build time reductions for packages like LibreOffice
 - `clang` enables you to instantiate templates of pre-compiled headers for even faster PCH builds

#### Smart and Simple Packaging

As well as making builds lightning quick, package files need to be just as easy to create. We achieve this through:

 - Build files are simple but powerful
   - Easy to understand the build at a glance
   - Able to quickly add extra compiler tunables to build
   - Plug in a workload to enable profile guided optimizations in your build. You don't even need to know how it works!
 - Tooling takes care of steps that can be easily automated
   - Will continue to learn new tricks to suit new workflows
 - Lets you know when you've likely made an error in packaging so you can fix it up before someone else sees
   - Reduce the need to rework commits

### Making Contributing Easy

Even the tiniest impediments to contributing are enough to turn off potential contributors. Most frictions contributors
face can be reduced, or even eliminated entirely.

Whether you simply need a few tools or enjoy the challenge, Serpent OS utilizes GitLab for packaging repositories.
There will also be community repositories where you can practice your packaging, share your experiences and eventually
start making changes upstream.
