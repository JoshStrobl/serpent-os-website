---
title: "Making Smart Decisions Behind the Scenes"
date: 2021-08-26T10:34:21+10:00
draft: false
Callout: "Solving problems before they become an issue"
---

Subscriptions make your experience on Serpent OS even better by handling packages and dependencies in a smarter way.
There's no longer a need to research how to get something working that your package manager should be smart enough to
handle. Subscriptions are as smart as the rules used to create them, so the community can create new ideas that will
make life easier for all users.

#### moss Knows What You Need

Subscriptions allow `moss` to create dependencies beyond the traditional 1-1 relationship that is typically used.
There is no need for optional or recommended dependencies to be used. Every distribution is different in terms of
package names and subscriptions mean you can start using your system without knowing much about how `moss` works
(though I highly recommend it for the curious types).

Here's some examples of how subscriptions can handle situations so you don't need to search for solutions:
 - Install 32bit graphics drivers when you have `steam` or `wine` installed. While this can happen automatically for
   the mesa drivers, it's not automatic for the `NVIDIA` drivers as installing them will break systems without
   supporting hardware. `moss` can make this dependency automatic for any system that has the 64bit graphics drivers
   installed.
 - Install the `dolphin` plugins for `nextcloud` when you have both programs installed, but don't install all the `KDE`
   dependencies of the plugins when you don't use `dolphin`. Rather than an optional dependency, this becomes a smart
   one while keeping the dependencies of each package complete.
 - Automatically install the `VA`/`VA-VPI` packages for your hardware, but not include all versions for all hardware.
 - Some software requires separate kernel modules, but how do you know which ones to install? Subscriptions take away
   the need for documentation. You choose the software, `moss` will handle fetching the right kernel modules.

#### Slim Down Your OS

Packages frequently include files that you may never use. `moss` can help reduce the cruft via subscriptions to
reduce unneeded dependencies, leading to a smaller OS footprint and slimmer updates.

Ways that `moss` can slim down your system:
 - Capability based subscriptions: This includes hardware specific requirements to get you the right drivers and
   modules. No longer do we need to include packages where we can obviously determine whether you will need (or not
   need) a package based on your installed hardware.
 - Locale based subscriptions: Language and translation files can take up quite a chunk of space on the system (nearly
   1 GB on my current machine) and aren't needed.
 - Documentation subscriptions: While documentation is very useful, many of us go straight to the search engine to
   read up on it online.

#### Customize Your System Without Rebuilding Packages

In theory, there's no limit to what you can achieve through subscriptions. By combining the efforts of our community,
subscriptions can open up plenty of variations that you may not have considered. As an example, you could opt into a
`minimal` subscription to installed slimmed versions of packages.

Here's a couple of ideas of how it might work, while integrating into upstream packages:
 - Split a package to separate files or plugins with large dependencies for features that you may not even need. Think
   of a plugin pulling in `QtWebEngine` just to display webpagea. A useful feature for some, but unneeded for others.
 - Provide a `minimal` experience for a desktop environment. This would be a smaller set of packages, but without the
   full features.
 - Reducing features would need to be opt-in. So users will be aware why some features would be missing.

There are many possibilities in how you can use subscriptions. While a powerful feature that you can customize, it also
just works out of the box without you even knowing it's there!
