---
title: "Taking Control of your Computing Experience"
date: 2021-06-21T22:08:34+10:00
draft: false
Callout: "Empowering users to make their own decisions, without getting in their way"
---

Serpent OS offers a plethora of tools to grant you flexibility in how you want to setup your computer. This gives you options to
get your software from a variety of sources including building your own packages. With all of our tools accessible to everyone,
it's really down to you how far you want to go.

Serpent OS is highly extensible, allowing you to create use cases we never dreamed of! Some features that will allow you to
customize your computer are:

 - A mixed source/binary distribution model.
 - A community maintained source repository of build files. You are free to make your own choice whether to use it and what
   packages to add to your Serpent OS base install.
 - Maintain your own repository, or submit your changes upstream with the support of a team of contributors.

#### Community Maintained Source Repository

Serpent OS allows users to easily build and package software that may not be available in the core repository (check out the [developer advantages](../contribute)). By enabling a shared repository, users can access the work of the rest of the community to conveniently add extra software. At some stage, this software could end up in the main repository!

To enable the best experience, Serpent OS tooling enforces a few requirements to ensure built software is of a higher quality.

- Linting of builds that can fail if doing something wrong with feedback to fix such problems
- Validation that the build fragments match the actual build (and not hand edited)
- Review of commits prior to merging (for contributors without access yet)
- Not conflicting with files from the core repository (with some exceptions like wine branches or mesa)

#### A Word of Caution
With great power comes great responsibility! While we take steps within tooling and processes to protect you and help you to
restore to a previous working state if things go wrong, there are some things which we do not do. Community reviews of updates
are to ensure correctness of the build and not the quality or value of the software contained. So sticking to software that is
well known will always be recommended.

The community repository will be separated from the Serpent OS core so that the team cannot prevent packages being added that
they may not see value in. That means it's up to you to make the right choices for the software you install on your system.
